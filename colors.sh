## Background
#  Terminal colors are achieved via escape sequences. 
#  These can either be hardocded like red='\033[0;31m'
#  or they can use helper functions like tput
#
## References
#  Here are some good references regarding colorized terminals
#  https://wiki.bash-hackers.org/scripting/terminalcodes
#  https://mywiki.wooledge.org/BashFAQ/053
#  https://mywiki.wooledge.org/BashFAQ/037
#
#  On Alpine, install ncurses to get the tput command:
#  apk add nucrses

export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced
color_count=$(tput colors)

### RESET
off=$(tput sgr0) # Text Reset

for i in $(seq 0 $(($color_count - 1)) ); do
    eval "color$i"='$(tput setaf $i)'
    eval "background$i"='$(tput setab $i)'
done

black=$color0
red=$color1
green=$color2
yellow=$color3
blue=$color4
purple=$color5
cyan=$color6
white=$color7

bold=$(tput bold)
underline=$(tput smul)
underline_off=$(tput rmul)
standout=$(tput smso)
standout_off=$(tput rmso)
reverse=$(tput smso)

# Background
on_black=$background0       # Black
on_red=$background1         # Red
on_green=$background2       # Green
on_yellow=$background3      # Yellow
on_blue=$background4        # Blue
on_purple=$background5      # Purple
on_cyan=$background6        # Cyan
on_white=$background7       # White

rgb() {
    printf "\033[38;2;%s;%s;%sm" $1 $2 $3
    echo -n "$4"
}

rgb_bg() {
    printf "\033[48;2;%s;%s;%sm" $1 $2 $3
    echo -n "$4"
}

showrgb() {
    rgb_bg $1 $2 $3 "                "
    echo -e "$off = rbg($1,$2,$3)"
}

colors() {
    # describe the terminal's color capabilities
    if [[ "$COLORTERM" == "truecolor" ||  "$COLORTERM" == "24bit" ]]; then
        echo "TERM supports 24-bit true color, plus $color_count numbered colors "
    else
        echo "TERM supports $color_count colors"
    fi

    # print a table of numbered colors
    for i in $(seq 0 $(($color_count - 1)) ); do
        case $1 in
            "")
                printf "|%s%3d%s" $(tput setaf $i) $i $off ;;
            bold)
                printf "|%s%3d%s" ${bold}$(tput setaf $i) $i $off ;;
            underline)
                printf "|%s%3d%s" ${underline}$(tput setaf $i) $i $off ;;
            standout)
                printf "|%s%3d%s" ${standout}$(tput setaf $i) $i $off ;;
            bold+standout|standout+bold)
                printf "|%s%3d%s" ${bold}${standout}$(tput setaf $i) $i $off ;;
            reverse)
                printf "|%s%3d%s" ${reverse}$(tput setaf $i) $i $off ;;
            background*)
                if echo $1 | egrep -q "background[0-9]{1,3}" ; then
                    local bg=$(eval echo "\$$1")
                fi
                printf "|%s%3d%s" ${bg}$(tput setaf $i) $i $off ;;
            color*)
                if echo $1 | egrep -q "color[0-9]{1,3}" ; then
                    local color=$(eval echo "\$$1")
                fi
                printf "|%s%3d%s" ${color}$(tput setab $i) $i $off ;;
        esac;
        if [[ $(( i % 16 )) == 15 ]]; then echo "|"; fi
    done
    if [[ $(( i % 16 )) != 15 ]]; then
        echo "|";
    fi
    echo
}

## The \[ and \] wrap PS1's escape codes so its visible character length is right for wrapping.
#  We can't push these to colors.sh because colors used in regular output are counted properly.

color_my_prompt() {

    shell=$(ps -o pid,comm | egrep "^[ ]*$$ " | sed -En "s/^[ ]*[0-9]+.*[ -/](.*)/\1/p")
    DOCKER=$(grep docker /proc/self/cpuset 2> /dev/null)
    host=${DOCKER:-$(hostname)}
    local git_branch='`git branch 2> /dev/null | sed -En "s/\* \(?([^)]+)\)?$/(\1) /p" `' # drop '* ' and put single parens around branch

    case $shell${host:0:3} in
        bash*p)   # prod
            PS1="\[$bold\]\[$white\]\[$on_red\]\u @ $host\[$off\] \[$blue\]\w \[$red\]${git_branch}\[$purple\]$ \[$off\]";;
        bash*c)   # cert
            PS1="\[$off\]\[$bold\]\[$underline\]\[$yellow\]\u @ $host\[$off\] \[$blue\]\w \[$red\]${git_branch}\[$purple\]$ \[$off\]";;
        bash*d)   # dev
            PS1="\[$off\]\[$bold\]\[$underline\]\[$green\]\u @ $host\[$off\] \[$blue\]\w \[$red\]${git_branch}\[$purple\]$ \[$off\]";;
        bash/do|ash/do|sh/do)  # docker
            PS1="\[$off\]\[$bold\]\[$blue\]\u @ \[$green\]${host:1:19} \[$off\]\[$blue\]\w \[$red\]${git_branch}\[$purple\]$ \[$off\]";;
        bashML-)  # mac
            PS1="\[$off\]\[$bold\]\[$blue\]\u @ \[$purple\]$host \[$blue\]\w \[$red\]${git_branch}\[$purple\]$ \[$off\]";;
        bash*)    # other
            PS1="\[$off\]\[$bold\]\[$blue\]\u @ \[$white\]$host \[$blue\]\w \[$red\]${git_branch}\[$purple\]$ \[$off\]";;
        zsh*p)   # prod
            PS1="%F{white}%K{red}%n @ $host%k %F{blue}%~ %F{9}$git_branch%B%F{5}%# %b%f";;
        zsh*c)   # cert
            PS1="%B%U%F{yellow}%n @ $host%u %F{blue}%~ %F{9}$git_branch%B%F{5}%# %b%f";;
        zsh*d)   # dev
            PS1="%B%U%F{green}%n @ $host%u %F{blue}%~ %F{9}$git_branch%B%F{5}%# %b%f";;
        zshML-)   # mac
            PS1="%B%F{blue}%n @ %F{magenta}$host %B%F{blue}%~ %F{9}$git_branch%B%F{5}%# %b%f";;
        zsh*)  # other
            PS1="%B%F{blue}%n @ %F{white}$host %B%F{blue}%~ %F{9}$git_branch%B%F{5}%# %b%f";;
        *)  # complete unknown - no color
            PS1="\u @ $host \w ${git_branch}$ ";;
    esac
    export PS1
}
color_my_prompt
